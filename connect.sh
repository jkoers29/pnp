#! /bin/bash

#########################################################################
#connect.sh, a bash script to connect to a WPA access point automagically
# first iteration by SDS for the PNP Capstone team
#written to run on Kali Linux rPi version
#########################################################################

wlan=wlan1

echo "Done!"

echo "Stopping mon0 and wlanX"
airmon-ng stop mon0
ifconfig $wlan down

echo "Done!"

echo "Bringing up wireless card"
ifconfig $wlan up
echo "Done!"


echo "Removing old config file and writing new one"
rm /etc/wpa_supplicant.conf
touch /etc/wpa_supplicant.conf

echo "network={" >> /etc/wpa_supplicant.conf
echo "    ssid=\"$1\" " >> /etc/wpa_supplicant.conf 
echo "    psk=\"$2\" " >> /etc/wpa_supplicant.conf
echo "}" >> /etc/wpa_supplicant.conf

echo "Done!"

echo "running wpa_supplicant"
wpa_supplicant  -B -D wext  -i $wlan  -c/etc/wpa_supplicant.conf
 
echo "Done!"

sleep 5

echo "Running dhclient"
dhclient -v $wlan
 
echo "Done!"

echo "checking ifconfig"
ifconfig $wlan

echo "Done!"

#ping -c5  8.8.8.8

exit

#Now we run Nmap, and hackem!
 






