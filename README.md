Product overview

		The Postal Network Penetrator (PNP) is designed to be a light-weight inexpensive 
	solution to gaining access to remote computers on a protected network. Built on 
	a platform assembled from readily available parts, this device will essentially 
	be able to be shipped to any location and autonomously start scanning wireless 
	networks to penetrate.
	
		PNP is battery operated, so it will need to be started remotely or from a 
	timer set on the battery before shipping. Once it is started it will 
	immediately start scanning wireless networks in its area. The device will 
	then gather and record network activity and run exploits to gain access to 
	those networks. Once in, it will then find remote machines to run additional 
	exploits on until it breaks through, and then it will collect all relevant 
	data from the compromised machine. The system will then transmit data to a secure 
	website that will display the information on the database in a clear and 
	understandable fashion.
	
		PNP will be assembled using a Raspberry Pi, an antenna, an SD card, a USB hub, 
	and a battery pack (all items are listed below). In addition, this project will 
	also need a USB device and a website for exporting data. Phase one uses a USB device 
	to capture data so we can view what has been captured. Ideally this is not what we 
	want as we might not ever see our device again if it gets lost in the mail, or never 
	returned. Phase two requires that a secure website be in place to capture the data 
	that the device is capturing. With this implementation we do not ever need to see 
	the device again as we will be capturing data while the device is in place. 
	
		For testing purposes we will need to set up an environment consisting of a 
	wireless access point and at least three computers with varying degrees of 
	security in place to create a mock network environment, much like what the device 
	will experience once shipped. 
	
		Kali linux will be installed on the PNP device and we will utilize the aircrack-ng 
	and	Metasploit libraries. We will need to create a Bash script that will automate the 
	manual steps that are required when hacking with the aircrack-ng suite. The script 
	will be automatically run at system startup and will run commands one at a time, 
	parsing	command results as it goes as some commands will rely on previous results to 
	be ran. 
	
		In addition to the Bash script several Metasploit modules will need to be created 
	to run exploits on compromised remote machines. These modules are written in the Ruby 
	programming	language and a host of them are already included in the Metasploit library. 
	However, some of these modules will need to be revised or entirely rewritten to tailor 
	to specific	needs, or to run different exploits when problems arise or some exploits 
	fail for various reasons.
	
		Below is a list of the hardware and software requirements for this project, and the 
	steps to create them.
		
	Hardware

	Equipment Schedule
	
	PNP Equipment
	Price (usd)
	Raspberry pi model B
	$35.00
	Raspberry pi case
	$9.99
	TP-LINK TL-WN822N High Gain Wireless N USB Adapter
	$14.99
	16GB Sony SDHC UH1 card
	$9.99
	50000mAh Backup External Battery USB Power Bank Pack
	$24.99
	Priority Mail Shipping Box
	$0.00
	Total cost
	$94.96
	
	
	Testing Equipment
	Price (usd)
	TP-LINK TL-WR841N Wireless Router (Research Lab)
	FREE
	Computer running Intrusion detection system (Research Lab)
	FREE
	Three test bed computers (Research Lab)
	FREE
	Network cables (Research Lab)
	FREE
	Cisco switch with port monitoring (Research Lab)
	FREE
	Total cost
	N/A
	 
	Software
	Software Schedule
	
	PNP Software
	Price (usd)
	Kali Linux Raspberry Pi 1.0.5
	$0.00
	Aircrack-ng 
	$0.00
	Airmon-ng
	$0.00
	Nmap network scanner
	$0.00
	Metasploit framework
	$0.00
	Total cost
	$0.00
	
	
	Testing Software
	Price (usd)
	Security onion
	$0.00
	Snort intrusion detection software
	$0.00
	Snorby
	$0.00
	Total cost
	$0.00	
	
	Packages Added to Kali Linux:
	
	ifrename
	VIM
	wpa_supplicant
	Nmap
	Metasploit
	 
